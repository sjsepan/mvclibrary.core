﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Serialization;
using Ssepan.Application.Core;
using Ssepan.Utility.Core;

namespace MvcLibrary.Core
{
    /// <summary>
    /// persisted settings; run-time model depends on this
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    [Serializable]
    public class MVCSettings :
        SettingsBase
    {
        #region Declarations
        private const string FILE_TYPE_EXTENSION = "mvcsettings";
        private const string FILE_TYPE_NAME = "mvcsettingsfile";
        private const string FILE_TYPE_DESCRIPTION = "MVC Settings File";
        #endregion Declarations

        #region Constructors
        static MVCSettings()
        {
            //NOTE: moved from instance ctor
            FileTypeExtension = FILE_TYPE_EXTENSION;
            FileTypeName = FILE_TYPE_NAME;
            FileTypeDescription = FILE_TYPE_DESCRIPTION;
            SerializeAs = SerializationFormat.Xml;//default
        }
        public MVCSettings()
        {
            SomeComponent = new MVCSettingsComponent();
        }

        public MVCSettings
        (
            int someInt,
            bool someBoolean,
            string someString
        ) :
            this()
        {
            SomeInt = someInt;
            SomeBoolean = someBoolean;
            SomeString = someString;
        }

        public MVCSettings
        (
            int someInt,
            bool someBoolean,
            string someString,
            int someOtherInt,
            bool someOtherBoolean,
            string someOtherString
        ) :
            this(someInt, someBoolean, someString)
        {
            SomeComponent.SomeOtherInt = someOtherInt;
            SomeComponent.SomeOtherBoolean = someOtherBoolean;
            SomeComponent.SomeOtherString = someOtherString;
        }

        public MVCSettings
        (
            int someInt,
            bool someBoolean,
            string someString,
            MVCSettingsComponent someComponent
        ) :
            this(someInt, someBoolean, someString)
        {
            SomeComponent = someComponent;
        }
        #endregion Constructors

        #region IDisposable support
        ~MVCSettings()
        {
            Dispose(false);
            //base.Finalize();//not called directly in C#; called by Destructor
        }

        //inherited; override if additional cleanup needed
        protected override void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                try
                {
                    //Resources not disposed
                    if (disposeManagedResources)
                    {
                        // dispose managed resources
                        SomeComponent = null;
                    }

                    disposed = true;
                }
                finally
                {
                    // dispose unmanaged resources
                    base.Dispose(disposeManagedResources);
                }
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion

        #region IEquatable<ISettingsComponent>
        /// <summary>
        /// Compare property values of two specified Settings objects.
        /// </summary>
        /// <param name="other">ISettingsComponent</param>
        /// <returns>bool</returns>
        public override bool Equals(ISettingsComponent other)
        {
            bool returnValue;
            try
            {
                MVCSettings otherSettings = other as MVCSettings;
                if (this == otherSettings)
                {
                    returnValue = true;
                }
                else if (!base.Equals(other))
                {
                    returnValue = false;
                }
                else if (!SomeComponent.Equals(otherSettings))
                {
                    returnValue = false;
                }
                else if (SomeInt != otherSettings.SomeInt)
                {
                    returnValue = false;
                }
                else if (SomeBoolean != otherSettings.SomeBoolean)
                {
                    returnValue = false;
                }
                else if (SomeString != otherSettings.SomeString)
                {
                    returnValue = false;
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }
        #endregion IEquatable<ISettingsComponent>

        #region Properties
        [XmlIgnore]
        public override bool Dirty
        {
            get
            {
                bool returnValue;
                try
                {
                    if (base.Dirty)
                    {
                        returnValue = true;
                    }
                    //else if (_SomeComponent.Equals(__SomeComponent))
                    else if (SomeComponent.Dirty)
                    {
                        returnValue = true;
                    }
                    else if (_SomeInt != __SomeInt)
                    {
                        returnValue = true;
                    }
                    else if (_SomeBoolean != __SomeBoolean)
                    {
                        returnValue = true;
                    }
                    else if (_SomeString != __SomeString)
                    {
                        returnValue = true;
                    }
                    else
                    {
                        returnValue = false;
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                    throw;
                }

                return returnValue;
            }
        }

        #region Persisted Properties
        //private MVCSettingsComponent __SomeComponent = default(MVCSettingsComponent);//not needed; individual properties are backed in settings
        private MVCSettingsComponent _SomeComponent;
        public MVCSettingsComponent SomeComponent
        {
            get { return _SomeComponent; }
            set
            {
                if (SettingsController<MVCSettings>.DefaultHandler != null && _SomeComponent != null)
                {
                    _SomeComponent.PropertyChanged -= SettingsController<MVCSettings>.DefaultHandler;
                }

                _SomeComponent = value;

                if (SettingsController<MVCSettings>.DefaultHandler != null && _SomeComponent != null)
                {
                    _SomeComponent.PropertyChanged += SettingsController<MVCSettings>.DefaultHandler;
                }

                OnPropertyChanged(nameof(SomeComponent));
            }
        }

        private int __SomeInt;
        private int _SomeInt;
        public int SomeInt
        {
            get { return _SomeInt; }
            set
            {
                _SomeInt = value;
                OnPropertyChanged(nameof(SomeInt));
            }
        }

        private bool __SomeBoolean;
        private bool _SomeBoolean;
        public bool SomeBoolean
        {
            get { return _SomeBoolean; }
            set
            {
                _SomeBoolean = value;
                OnPropertyChanged(nameof(SomeBoolean));
            }
        }

        private string __SomeString = string.Empty;//default(string);
        private string _SomeString = string.Empty;//default(string);
        public string SomeString
        {
            get { return _SomeString; }
            set
            {
                _SomeString = value;
                OnPropertyChanged(nameof(SomeString));
            }
        }
        #endregion Persisted Properties
        #endregion Properties

        #region Methods
        /// <summary>
        /// Copies property values from source working fields to destination working fields, then optionally syncs destination.
        /// </summary>
        /// <param name="destination">ISettingsComponent</param>
        /// <param name="sync">bool</param>
        public override void CopyTo(/*ISettings*/ISettingsComponent destination, bool sync)
        {
            try
            {
                MVCSettings destinationSettings = destination as MVCSettings;

                //destinationSettings.SomeComponent = SomeComponent;
                SomeComponent.CopyTo(destinationSettings.SomeComponent, sync);

                destinationSettings.SomeInt = SomeInt;
                destinationSettings.SomeBoolean = SomeBoolean;
                destinationSettings.SomeString = SomeString;

                base.CopyTo(destination, sync);//also checks and optionally performs sync
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Syncs property values by copying from working fields to reference fields.
        /// </summary>
        public override void Sync()
        {
            try
            {
                //__SomeComponent = ObjectHelper.Clone<MVCSettingsComponent>(_SomeComponent);
                SomeComponent.Sync();

                __SomeInt = _SomeInt;
                __SomeBoolean = _SomeBoolean;
                __SomeString = _SomeString;

                base.Sync();

                if (Dirty)
                {
                    throw new ApplicationException("Sync failed.");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion Methods
    }
}
