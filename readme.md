# readme.md - README for MvcLibrary.Core v0.6

## About

App-specific library of application functions and classes for C# .Net Core applications; requires Ssepan.Application.Core, ssepan.io.core, ssepan.utility.core

### Purpose

To serve as a reference architecture and template for console and WinForms apps that share a common model and settings file.
Also demonstrates my interpretation of the Model-View-Controller pattern.

### Usage notes

~N/A

### Enhancements

~TODO:modify forms interface to include more standard menus/buttons/actions
~TODO:review design around args param, and whether it should go into view (now that model is not init until view loaded)

### History

1.0:
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting

0.6:
~Changes to client app resulting from refactoring of Ssepan.Application.Core[.*].
~Added license.txt file.
~renamed readme

0.5:
~Using Ssepan.Core.* v0.5
~Modified to remove dependency on EventLogEntryType in System.Diagnostics. Logging will be written to StdErr. If you want to redirect errors to a log, use a command like 'dotnet run 2>app.log' in the app folder.

0.4:
~Ported to .Net (Core) 5/6
~Modified app stack to include and use support for JSON-formatted settings files, instead of the default (XML).
~Created a Sample folder in this project containing sample settings files matching the app.config settings, as both the GUI adn Console app automatically load them. Files must be copied to location corresponding to Environment.SpecialFolder.Personal before running programs.

0.3:
~Handlers were not being correctly wired up to sub-components of Settings or Model
~Using Ssepan.Application 2.8
~Added ModelCompoment sub-component to Model, to demonstrate notification from component NOT backed by Settings or SettingsComponent (which DO perform notification internally).

0.2:
~Handler in Program.cs was not wired up in forms or console; added 'PropertyChanged += PropertyChangedEventHandlerDelegate;' to Main.
~No handler wired up to Settings in forms or console. Renamed 'PropertyChangedEventHandlerDelegate' to 'ModelPropertyChangedEventHandlerDelegate'. Wired up latter in 'InitViewModel' after model handler in view. Currently handling 'Dirty' property.
~Added 'SettingsPropertyChangedEventHandlerDelegate' to new static property 'DefaultHandler' in SettingsController. 
~Modified OnPropertyChanged in SettingsBase to fire OnChanged for 'Dirty' when property name is not 'Dirty'.
~Fixed bug in GetPathForSave in Ssepan.Io where SaveAs did not display dialog for '(new)'.
~Modified Settings in Ssepan.Application to implement part of its interfaces as a new interface ISettingsComponent, and implemented an example as a property SomeComponent which copies the PropertyChanged handlers from settings and implements its own Equals, Dirty, CopyTo, Sync, etc, so Settings does not need to know the details.
~Updated to Framework 4.8
~Updated Ssepan.* to 2.7

0.1: (RELEASED)
~Refactored to use new MVVM / MVC hybrid.
~Updated Ssepan.* to 2.6

### Fixes

~N/A

### Known Issues

~

### Possible Enhancements

~n/a

Steve Sepan
sjsepan@yahoo.com
2/28/2024
