﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Reflection;
using Ssepan.Application.Core;
using Ssepan.Utility.Core;

namespace MvcLibrary.Core
{
    /// <summary>
    /// component of persisted settings;
    /// run-time model depends on this;
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    [Serializable]
    public class MVCSettingsComponent :
        SettingsComponentBase
    {
        #region Constructors
        public MVCSettingsComponent()
        {
        }

        public MVCSettingsComponent
        (
            int someOtherInt,
            bool someOtherBoolean,
            string someOtherString
        ) :
            this()
        {
            SomeOtherInt = someOtherInt;
            SomeOtherBoolean = someOtherBoolean;
            SomeOtherString = someOtherString;
        }
        #endregion Constructors

        #region IDisposable support
        ~MVCSettingsComponent()
        {
            Dispose(false);
        }

        //inherited; override if additional cleanup needed
        protected override void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                try
                {
                    //Resources not disposed
                    if (disposeManagedResources)
                    {
                        // dispose managed resources
                    }

                    disposed = true;
                }
                finally
                {
                    // dispose unmanaged resources
                    base.Dispose(disposeManagedResources);
                }
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion

        #region IEquatable<ISettingsComponent>
        /// <summary>
        /// Compare property values of two specified Settings objects.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(ISettingsComponent other)
        {
            bool returnValue;
            try
            {
                MVCSettingsComponent otherModel = other as MVCSettingsComponent;
                if (this == otherModel)
                {
                    returnValue = true;
                }
                else
                {
                    if (!base.Equals(other))
                    {
                        returnValue = false;
                    }
                    else if (this.SomeOtherInt != otherModel.SomeOtherInt)
                    {
                        returnValue = false;
                    }
                    else if (this.SomeOtherBoolean != otherModel.SomeOtherBoolean)
                    {
                        returnValue = false;
                    }
                    else if (this.SomeOtherString != otherModel.SomeOtherString)
                    {
                        returnValue = false;
                    }
                    else
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }
        #endregion IEquatable<ISettingsComponent>

        #region Properties
        [XmlIgnore]
        public override bool Dirty
        {
            get
            {
                bool returnValue;
                try
                {
                    if (base.Dirty)
                    {
                        returnValue = true;
                    }
                    else if (_SomeOtherInt != __SomeOtherInt)
                    {
                        returnValue = true;
                    }
                    else if (_SomeOtherBoolean != __SomeOtherBoolean)
                    {
                        returnValue = true;
                    }
                    else if (_SomeOtherString != __SomeOtherString)
                    {
                        returnValue = true;
                    }
                    else
                    {
                        returnValue = false;
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                    throw;
                }

                return returnValue;
            }
        }

        #region Persisted Properties
        private int __SomeOtherInt;
        private int _SomeOtherInt;
        public int SomeOtherInt
        {
            get { return _SomeOtherInt; }
            set
            {
                _SomeOtherInt = value;
                OnPropertyChanged(nameof(SomeOtherInt));
            }
        }

        private bool __SomeOtherBoolean;
        private bool _SomeOtherBoolean;
        public bool SomeOtherBoolean
        {
            get { return _SomeOtherBoolean; }
            set
            {
                _SomeOtherBoolean = value;
                OnPropertyChanged(nameof(SomeOtherBoolean));
            }
        }

        private string __SomeOtherString = string.Empty;//default(string);
        private string _SomeOtherString = string.Empty;//default(string);
        public string SomeOtherString
        {
            get { return _SomeOtherString; }
            set
            {
                _SomeOtherString = value;
                OnPropertyChanged(nameof(SomeOtherString));
            }
        }
        #endregion Persisted Properties
        #endregion Properties

        #region Methods
        /// <summary>
        /// Copies property values from source working fields to destination working fields, then optionally syncs destination.
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="sync"></param>
        public override void CopyTo(ISettingsComponent destination, bool sync)
        {
            try
            {
                MVCSettingsComponent destinationSettings = destination as MVCSettingsComponent;
                destinationSettings.SomeOtherInt = this.SomeOtherInt;
                destinationSettings.SomeOtherBoolean = this.SomeOtherBoolean;
                destinationSettings.SomeOtherString = this.SomeOtherString;

                base.CopyTo(destination, sync);//also checks and optionally performs sync
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Syncs property values by copying from working fields to reference fields.
        /// </summary>
        public override void Sync()
        {
            try
            {
                __SomeOtherInt = _SomeOtherInt;
                __SomeOtherBoolean = _SomeOtherBoolean;
                __SomeOtherString = _SomeOtherString;

                base.Sync();

                if (Dirty)
                {
                    throw new ApplicationException("Sync failed.");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion Methods
    }
}
