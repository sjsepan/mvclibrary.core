﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using Ssepan.Application.Core;
using Ssepan.Utility.Core;

namespace MvcLibrary.Core
{
    /// <summary>
    /// run-time model; relies on settings
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class MVCModel :
        ModelBase
    {
        #region Constructors
        public MVCModel()
        {
            //init SomeComponent property backed by setting component
            if (SettingsController<MVCSettings>.Settings == null)
            {
                //ensures that there is a new instance of Settings backing persisted properties in model
                SettingsController<MVCSettings>.New();
            }
            Debug.Assert(SettingsController<MVCSettings>.Settings != null, "SettingsController<MVCSettings>.Settings != null");

            //init some other component property NOT backed by settings, but backed by model component
           StillAnotherComponent = new MVCModelComponent();
        }

        public MVCModel
        (
            int someInt,
            bool someBoolean,
            string someString
        ) :
            this()
        {
            SomeInt = someInt;
            SomeBoolean = someBoolean;
            SomeString = someString;
        }

        public MVCModel
        (
            int someInt,
            bool someBoolean,
            string someString,
            int someOtherInt,
            bool someOtherBoolean,
            string someOtherString
        ) :
            this(someInt,someBoolean,someString)
        {
            SomeComponent.SomeOtherInt = someOtherInt;
            SomeComponent.SomeOtherBoolean = someOtherBoolean;
            SomeComponent.SomeOtherString = someOtherString;
        }

        public MVCModel
        (
            int someInt,
            bool someBoolean,
            string someString,
            int someOtherInt,
            bool someOtherBoolean,
            string someOtherString,
            int stillAnotherInt,
            bool stillAnotherBoolean,
            string stillAnotherString
        ) :
            this(someInt,someBoolean,someString, someOtherInt, someOtherBoolean, someOtherString)
        {
            StillAnotherComponent.StillAnotherInt = stillAnotherInt;
            StillAnotherComponent.StillAnotherBoolean = stillAnotherBoolean;
            StillAnotherComponent.StillAnotherString = stillAnotherString;
        }
        #endregion Constructors

        #region IDisposable support
        ~MVCModel()
        {
            Dispose(false);
            //base.Finalize();//not called directly in C#; called by Destructor
        }

        //inherited; override if additional cleanup needed
        protected override void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                try
                {
                    //Resources not disposed
                    if (disposeManagedResources)
                    {
                        // dispose managed resources
                        StillAnotherComponent = null;
                    }

                    disposed = true;
                }
                finally
                {
                    // dispose unmanaged resources
                    base.Dispose(disposeManagedResources);
                }
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion

        #region IEquatable<IModel>
        /// <summary>
        /// Compare property values of two specified Model objects.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(IModelComponent other)
        {
            bool returnValue;
            try
            {
                MVCModel otherModel = other as MVCModel;
                if (this == otherModel)
                {
                    returnValue = true;
                }
                else
                {
                    if (!base.Equals(other))
                    {
                        returnValue = false;
                    }
                    else if (SomeInt != otherModel.SomeInt)
                    {
                        returnValue = false;
                    }
                    else if (SomeBoolean != otherModel.SomeBoolean)
                    {
                        returnValue = false;
                    }
                    else if (SomeString != otherModel.SomeString)
                    {
                        returnValue = false;
                    }
                    else
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }
        #endregion IEquatable<IModel>

        #region Properties
        private string[] _Args;
        public string[] Args
        {
            get {  return _Args; }
            set
            {
                _Args = value;
                OnPropertyChanged(nameof(Args));
            }
        }

        public MVCSettingsComponent SomeComponent
        {
            get { return SettingsController<MVCSettings>.Settings.SomeComponent; }
            set
            {
                SettingsController<MVCSettings>.Settings.SomeComponent = value;

                //OnPropertyChanged("SomeComponent");//not needed if fired by settings
            }
        }

        private MVCModelComponent _StillAnotherComponent;
        public MVCModelComponent StillAnotherComponent
        {
            get { return _StillAnotherComponent; }
            set
            {
                if (ModelController<MVCModel>.DefaultHandler != null)
                {
                    if (_StillAnotherComponent != null)
                    {
                        _StillAnotherComponent.PropertyChanged -= ModelController<MVCModel>.DefaultHandler;
                    }
                }

                _StillAnotherComponent = value;

                if (ModelController<MVCModel>.DefaultHandler != null)
                {
                    if (_StillAnotherComponent != null)
                    {
                        _StillAnotherComponent.PropertyChanged += ModelController<MVCModel>.DefaultHandler;
                    }
                }

                OnPropertyChanged(nameof(StillAnotherComponent));//needed because NOT backed by settings
            }
        }

        public int SomeInt
        {
            get { return SettingsController<MVCSettings>.Settings.SomeInt; }
            set
            {
                SettingsController<MVCSettings>.Settings.SomeInt = value;
                //OnPropertyChanged("SomeInt");//not needed if fired by settings
            }
        }

        public bool SomeBoolean
        {
            get { return SettingsController<MVCSettings>.Settings.SomeBoolean; }
            set
            {
                SettingsController<MVCSettings>.Settings.SomeBoolean = value;
                //OnPropertyChanged("SomeBoolean");//not needed if fired by settings
            }
        }

        public string SomeString
        {
            get { return SettingsController<MVCSettings>.Settings.SomeString; }
            set
            {
                SettingsController<MVCSettings>.Settings.SomeString = value;
                //OnPropertyChanged("SomeString");//not needed if fired by settings
            }
        }
        #endregion Properties

    }
}
