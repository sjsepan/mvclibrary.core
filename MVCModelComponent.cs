﻿using System;
using System.ComponentModel;
using System.Reflection;
using Ssepan.Application.Core;
using Ssepan.Utility.Core;

namespace MvcLibrary.Core
{
    /// <summary>
    /// component of non-persisted properties;
    /// run-time model depends on this;
    /// does NOT rely on settings
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class MVCModelComponent :
        ModelComponentBase
    {
        #region Constructors
        public MVCModelComponent()
        {
        }

        public MVCModelComponent
        (
            int stillAnotherInt,
            bool stillAnotherBoolean,
            string stillAnotherString
        ) :
            this()
        {
            StillAnotherInt = stillAnotherInt;
            StillAnotherBoolean = stillAnotherBoolean;
            StillAnotherString = stillAnotherString;
        }
        #endregion Constructors

        #region IDisposable support
        ~MVCModelComponent()
        {
            Dispose(false);
        }

        //inherited; override if additional cleanup needed
        protected override void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                try
                {
                    //Resources not disposed
                    if (disposeManagedResources)
                    {
                        // dispose managed resources
                    }

                    disposed = true;
                }
                finally
                {
                    // dispose unmanaged resources
                    base.Dispose(disposeManagedResources);
                }
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion

        #region IEquatable<IModelComponent>
        /// <summary>
        /// Compare property values of two specified Settings objects.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(IModelComponent other)
        {
            bool returnValue;
            try
            {
                MVCModelComponent otherModel = other as MVCModelComponent;
                if (this == otherModel)
                {
                    returnValue = true;
                }
                else
                {
                    if (!base.Equals(other))
                    {
                        returnValue = false;
                    }
                    else if (this.StillAnotherInt != otherModel.StillAnotherInt)
                    {
                        returnValue = false;
                    }
                    else if (this.StillAnotherBoolean != otherModel.StillAnotherBoolean)
                    {
                        returnValue = false;
                    }
                    else if (this.StillAnotherString != otherModel.StillAnotherString)
                    {
                        returnValue = false;
                    }
                    else
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }
        #endregion IEquatable<IModelComponent>

        #region Properties

        #region Non-Persisted Properties
        private int _StillAnotherInt;
        public int StillAnotherInt
        {
            get { return _StillAnotherInt; }
            set
            {
                _StillAnotherInt = value;
                OnPropertyChanged(nameof(StillAnotherInt));
            }
        }

        private bool _StillAnotherBoolean;
        public bool StillAnotherBoolean
        {
            get { return _StillAnotherBoolean; }
            set
            {
                _StillAnotherBoolean = value;
                OnPropertyChanged(nameof(StillAnotherBoolean));
            }
        }

        private string _StillAnotherString = string.Empty;//default(string);
        public string StillAnotherString
        {
            get { return _StillAnotherString; }
            set
            {
                _StillAnotherString = value;
                OnPropertyChanged(nameof(StillAnotherString));
            }
        }
        #endregion Non-Persisted Properties
        #endregion Properties

    }
}
